(setq debug-on-error t)
;; Setting rbenv path

(setenv "PATH" (concat (getenv "HOME") "/.rbenv/shims:" (getenv "HOME") "/.rbenv/bin:" (getenv "PATH")))
(setq exec-path (cons (concat (getenv "HOME") "/.rbenv/shims") (cons (concat (getenv "HOME") "/.rbenv/bin") exec-path)))
(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
        ("melpa" . "http://melpa.milkbox.net/packages/")
        ))

(package-initialize)


;; (load-theme 'solarized-light t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#eee8d5" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#839496"])
 '(beacon-color "#dc322f")
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#657b83")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-enabled-themes (quote (sanityinc-solarized-dark)))
 '(custom-safe-themes
   (quote
    ("8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "0598c6a29e13e7112cfbc2f523e31927ab7dce56ebb2016b567e1eff6dc1fd4f" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" "4cf3221feff536e2b3385209e9b9dc4c2e0818a69a1cdb4b522756bcdf4e00a4" "f350c66dcff6db73192c4819363b7c1992931841e5ab381b1ed57ef8257a498f" "bad832ac33fcbce342b4d69431e7393701f0823a3820f6030ccc361edd2a4be4" "4eaad15465961fd26ef9eef3bee2f630a71d8a4b5b0a588dc851135302f69b16" "9370aeac615012366188359cb05011aea721c73e1cb194798bc18576025cabeb" "cb453c60201288b198d115f1121fdab94d609da418cc6d0775ee0dea5beab5ef" "d2622a2a2966905a5237b54f35996ca6fda2f79a9253d44793cfe31079e3c92b" "71b172ea4aad108801421cc5251edb6c792f3adbaecfa1c52e94e3d99634dee7" "9f5fe6191b981ce29a2b4f8e4dbcefef7dd33b292d80c620f754be174efa9d58" default)))
 '(fci-rule-color "#eee8d5")
 '(frame-background-mode (quote dark))
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#fdf6e3" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#586e75")
 '(highlight-tail-colors
   (quote
    (("#eee8d5" . 0)
     ("#B4C342" . 20)
     ("#69CABF" . 30)
     ("#69B7F0" . 50)
     ("#DEB542" . 60)
     ("#F2804F" . 70)
     ("#F771AC" . 85)
     ("#eee8d5" . 100))))
 '(hl-bg-colors
   (quote
    ("#DEB542" "#F2804F" "#FF6E64" "#F771AC" "#9EA0E5" "#69B7F0" "#69CABF" "#B4C342")))
 '(hl-fg-colors
   (quote
    ("#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3")))
 '(hl-paren-colors (quote ("#2aa198" "#b58900" "#268bd2" "#6c71c4" "#859900")))
 '(js-indent-level 2)
 '(magit-diff-use-overlays nil)
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(org-support-shift-select t)
 '(package-selected-packages
   (quote
    (flyspell-correct diff-hl haml-mode flymake-go flymake-go-staticcheck flymake-haml flymake-json flymake-yaml go-mode markdown-mode rbenv scss-mode yaml-mode rubocopfmt rubocop js-auto-beautify rjsx-mode neotree flycheck-jest jest kubernetes vue-mode highlight-numbers highlight-parentheses rainbow-delimiters eslint-fix eslintd-fix react-snippets editorconfig editorconfig-charset-extras editorconfig-custom-majormode editorconfig-domain-specific editorconfig-generate enh-ruby-mode vue-html-mode lorem-ipsum flycheck flycheck-css-colorguard company zencoding-mode web-mode tidy ruby-tools ruby-refactor ruby-electric ruby-dev ruby-block ruby-additional rinari rainbow-mode php-refactor-mode php-mode magit-find-file magit-filenotify jsx-mode json-mode jsfmt js2-refactor js2-highlight-vars iedit ido-ubiquitous flymake-ruby color-theme-solarized color-theme-sanityinc-solarized)))
 '(pos-tip-background-color "#eee8d5")
 '(pos-tip-foreground-color "#586e75")
 '(show-paren-mode t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#eee8d5" 0.2))
 '(syslog-debug-face
   (quote
    ((t :background unspecified :foreground "#2aa198" :weight bold))))
 '(syslog-error-face
   (quote
    ((t :background unspecified :foreground "#dc322f" :weight bold))))
 '(syslog-hour-face (quote ((t :background unspecified :foreground "#859900"))))
 '(syslog-info-face
   (quote
    ((t :background unspecified :foreground "#268bd2" :weight bold))))
 '(syslog-ip-face (quote ((t :background unspecified :foreground "#b58900"))))
 '(syslog-su-face (quote ((t :background unspecified :foreground "#d33682"))))
 '(syslog-warn-face
   (quote
    ((t :background unspecified :foreground "#cb4b16" :weight bold))))
 '(term-default-bg-color "#fdf6e3")
 '(term-default-fg-color "#657b83")
 '(tool-bar-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#c85d17")
     (60 . "#be730b")
     (80 . "#b58900")
     (100 . "#a58e00")
     (120 . "#9d9100")
     (140 . "#959300")
     (160 . "#8d9600")
     (180 . "#859900")
     (200 . "#669b32")
     (220 . "#579d4c")
     (240 . "#489e65")
     (260 . "#399f7e")
     (280 . "#2aa198")
     (300 . "#2898af")
     (320 . "#2793ba")
     (340 . "#268fc6")
     (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#fdf6e3" "#eee8d5" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#657b83" "#839496")))
 '(xterm-color-names
   ["#eee8d5" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#073642"])
 '(xterm-color-names-bright
   ["#fdf6e3" "#cb4b16" "#93a1a1" "#839496" "#657b83" "#6c71c4" "#586e75" "#002b36"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Ubuntu Mono" :foundry "DAMA" :slant normal :weight normal :height 121 :width normal))))
 '(speedbar-button-face ((t nil))))


;; Interactively Do Things (highly recommended, but not strictly required)
(require 'ido)
(ido-mode t)



;; Alper
;;(global-set-key "\M- " 'hippie-expand)
(winner-mode 1)
(setq org-todo-keywords
      '((sequence "TODO" "WAITING" "TESTING" "|" "DONE" "REJECTED")))

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(recentf-mode 1)
(line-number-mode 1)


(require 'linum)
(global-linum-mode)
(setq make-backup-files nil)

(define-key isearch-mode-map (kbd "C-o")
  (lambda ()
    (interactive)
    (let ((case-fold-search isearch-case-fold-search))
      (occur (if isearch-regexp isearch-string
               (regexp-quote isearch-string))))))

(recentf-mode 1)



(defun kill-other-buffers ()
  "kill other buffers"
  (interactive)
  (dolist (buffer (buffer-list)) (unless (eql buffer (current-buffer)) (kill-buffer buffer)))
  )

(provide 'kill-other-buffers)
(global-set-key "\C-x\C-k\C-k" 'kill-other-buffers)


(setq inferior-lisp-program "/usr/bin/sbcl")
(setq inferior-scheme-program "/usr/bin/racket")



(defun duplicate-line (arg)
  "Duplicate current line, leaving point in lower line."
  (interactive "*p")

  ;; save the point for undo
  (setq buffer-undo-list (cons (point) buffer-undo-list))

  ;; local variables for start and end of line
  (let ((bol (save-excursion (beginning-of-line) (point)))
        eol)
    (save-excursion

      ;; don't use forward-line for this, because you would have
      ;; to check whether you are at the end of the buffer
      (end-of-line)
      (setq eol (point))

      ;; store the line and disable the recording of undo information
      (let ((line (buffer-substring bol eol))
            (buffer-undo-list t)
            (count arg))
        ;; insert the line arg times
        (while (> count 0)
          (newline)         ;; because there is no newline in 'line'
          (insert line)
          (setq count (1- count)))
        )

      ;; create the undo information
      (setq buffer-undo-list (cons (cons eol (point)) buffer-undo-list)))
    ) ; end-of-let

  ;; put the point in the lowest line and return
  (next-line arg))

(global-set-key (kbd "\C-c\C-d") 'duplicate-line)

(setq inhibit-startup-message t)
(fset 'yes-or-no-p 'y-or-n-p)
(setq-default truncate-lines t)


;;(windmove-default-keybindings 'control)
(add-to-list 'load-path "~/.emacs.d/elisp")
(require 'iedit)
;; (require 'chuck-mode)
(require 'android-mode)
(define-key global-map (kbd "C-;") 'iedit-mode)
(define-key isearch-mode-map (kbd "C-;") 'iedit-mode)

;; (add-hook 'after-init-hook #'global-flycheck-mode)



;; Prevent the cursor from blinking
(blink-cursor-mode 0)
;; Don't use messages that you don't read
(setq initial-scratch-message "")
(setq inhibit-startup-message t)
;; Don't let Emacs hurt your ears
(setq visible-bell t)
;; Who use the bar to scroll?
(scroll-bar-mode 0)
(tool-bar-mode 0)


;;(speedbar 1)
;;(kill-other-buffers)
(dired "~")

(projectile-global-mode)
(add-hook 'after-init-hook 'global-company-mode)


(put 'dired-find-alternate-file 'disabled nil)

(autoload 'moz-minor-mode "moz" "Mozilla Minor and Inferior Mozilla Modes" t)
(add-hook 'js2-mode-hook 'js2-custom-setup)
(defun js2-custom-setup () (moz-minor-mode 1))


;; (require 'turkish)



(add-to-list 'auto-mode-alist '("\\.htm\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
)
(add-hook 'web-mode-hook  'my-web-mode-hook)

(yas-global-mode 1)

(lorem-ipsum-use-default-bindings)



(defun my-setup-indent (n)
  ;; java/c/c++
  ;; (setq-local c-basic-offset n)
  ;; web development
  ;; (setq-local python-indent-offset n)
  (setq-local javascript-indent-level n) ; javascript-mode
  (setq-local js-indent-level n) ; js-mode
  (setq-local js2-basic-offset n) ; js2-mode, in latest js2-mode, it's alias of js-indent-level
  (setq-local web-mode-markup-indent-offset n) ; web-mode, html tag in html file
  (setq-local web-mode-css-indent-offset n) ; web-mode, css in html file
  (setq-local web-mode-code-indent-offset n) ; web-mode, js code in html file
  (setq-local css-indent-offset n) ; css-mode
  )

(my-setup-indent 2) ;


(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c C-p") 'projectile-command-map)


(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook 'rainbow-mode)
(add-hook 'web-mode-hook 'zencoding-mode)


(add-hook 'php-mode-hook 'my-php-mode-hook)
(defun my-php-mode-hook ()
  (setq indent-tabs-mode t)
  (let ((my-tab-width 4))
    (setq tab-width my-tab-width)
    (setq c-basic-indent my-tab-width)
    (set (make-local-variable 'tab-stop-list)
         (number-sequence my-tab-width 200 my-tab-width))))


(use-package ruby-mode
  :ensure t
  :config
    (auto-complete-mode)
    (add-to-list 'auto-mode-alist '("Capfile" . ruby-mode))
    (add-to-list 'auto-mode-alist '("Gemfile" . ruby-mode))
    (add-to-list 'auto-mode-alist '("Rakefile" . ruby-mode))
    (add-to-list 'auto-mode-alist '("\\.rake\\'" . ruby-mode))
    (add-to-list 'auto-mode-alist '("\\.rb\\'" . ruby-mode))
    (add-to-list 'auto-mode-alist '("\\.ru\\'" . ruby-mode)))

(use-package rubocop
  :ensure t
  :config
    (add-hook 'ruby-mode-hook 'rubocop-mode)
    (setq rubocop-check-command "rubocop --format emacs --config .rubocop.yml")
    (setq rubocop-autocorrect-command "rubocop -a --format emacs --config .rubocop.yml")
    ;; (setq rubocop-autocorrect-on-save 0)
    )

(add-hook 'rubocop-mode-hook
        (lambda ()
          (make-variable-buffer-local 'flycheck-command-wrapper-function)
          (setq flycheck-command-wrapper-function
                (lambda (command)
                  (append '("bundle" "exec") command)))))


;; (use-package rbenv
;;   :ensure t
;;   :config (global-rbenv-mode))


(use-package go-mode
  :ensure t
  :config
    (setq-default tab-width 2)
    (setq indent-tabs-mode 1)
    (setq gofmt-command "~/go/bin/goimports")
    ;; (add-hook 'before-save-hook 'gofmt-before-save)
    (auto-complete-mode))


(use-package flycheck
  :ensure t
  :config (global-flycheck-mode))

(use-package web-mode
  :ensure t
  :config (setq web-mode-content-types-alist
                '(("jsx"  . ".*\\.js.es6")))
  :mode (("\\.html$"    . web-mode)
         ("\\.erb$"     . web-mode)
         ("\\.hbs$"     . web-mode)
         ("\\.js.es6$"  . web-mode)))


(use-package haml-mode
	     :ensure t)

(use-package markdown-mode
  :ensure t
  :mode (("\\.md" . markdown-mode)))

(use-package yaml-mode
  :ensure t)

(use-package scss-mode
	     :ensure t)

;; (add-hook 'before-save-hook 'delete-trailing-whitespace)



(use-package diff-hl
  :ensure t
  :config
  (global-diff-hl-mode))

(setq-default indent-tabs-mode nil)


(defcustom laramie-standard-indent 2
  "The default value for indentation."
  :type 'integer)
(setq-default standard-indent     laramie-standard-indent
              css-indent-offset   laramie-standard-indent
              js-indent-level     laramie-standard-indent)

(show-paren-mode 1)

